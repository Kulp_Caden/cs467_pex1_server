// UDPServerFuncts.c

//
// Created by C21Caden.Kulp on 9/3/2019.
//

#include "UDPServerFuncts.h"
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>





int CalcFrameSize(unsigned char byte)     {

    //Want to look at the byte that corresponds to letters EEEEFFGH (the third byte) on the docx
    //This byte contains information to calculate bit rate

    int frameSize;

    //This was only used to test if the function worked
    //uint8_t test = 186;   //1011 1010

    unsigned char padByte = byte;
    int paddingBit = ((padByte >> 1) & 1);     //Isolates padding bit from frame

    //this maps the 4 bit binary value from mp3 header to a bitrate
    int bitrate_lookup[] = {0, 32, 40, 48, 56, 64, 80, 96, 112, 128, 160, 192, 224, 256, 320, -1};
    unsigned char bitrateByte = byte;
    int bitRate = bitrate_lookup[((bitrateByte >> 4) & 15)];   //isolates bitrate index from frame

    // This maps the 2 bit binary value from mp3 header to samplerate
    int samplerate_lookup[] = {44100, 48000, 32000, -1};
    unsigned char sampleByte = byte;
    int sampleRate = samplerate_lookup[((sampleByte >> 2) & 3)];  //isolates sample rate index from frame

    if (paddingBit == 0)     {
        frameSize = (144 * (bitRate * 1000)) / sampleRate;
    }
    else    {
        frameSize = ((144 * (bitRate * 1000)) / sampleRate) + 1;
    }

    return frameSize;
}


void readMP3(char* filename, int socketfd, struct sockaddr_in cliaddr, int len ) {
    FILE* songfp;
    int readfile = 1;
    songfp = fopen(filename, "r");
    if (songfp == NULL) {
        printf("error reading file\n");
        readfile = 0;
        //exit(-1);
    }

    // while loop vars
    char message[2121]; //the message we send shouldn't be more than this
    int frameSize;
    unsigned char byte; //1 byte
    int frameNo = 1;

    // https://stackoverflow.com/questions/1835986/how-to-use-eof-to-run-through-a-text-file-in-c
    // talked to Brynn Sulte about what
    while(!feof(songfp) ) { // does this work to read all the file

        byte = fgetc(songfp);
        if (byte == 0xff) {
            byte = fgetc(songfp);
            if (!feof(songfp)) {
                readfile = 0;
            }
            if (byte == 0xfb && readfile != 0) {
                byte = fgetc(songfp);
                frameSize = CalcFrameSize(byte);

                fprintf(stdin, "Frame #%d Found! Size: %d bytes.  Location in file: %d", frameNo, frameSize, SEEK_CUR);
                frameNo++;

                //rewind the filepointer 3 bytes to make sure ff, fb, and the next char are added to the data we are sending
                // https://www.geeksforgeeks.org/fseek-in-c-with-example/
                fseek(songfp, -3, SEEK_CUR);

                strcpy(message, "STREAM_DATA\n");

                for (int i = 0; i < frameSize; i++) {
                    message[strlen("STREAM_DATA\n") + i] = fgetc(songfp);

                }
                // send the frame
                sendto(socketfd, (const char *) message, (strlen("STREAM_DATA\n") + frameSize), 0, (const struct sockaddr *) &cliaddr, len);
                usleep(1);
            } else {
                if (!feof(songfp)) {
                    fseek(songfp, -1, SEEK_CUR);
                }
            }

        }

    }
    fclose(songfp);
    char streamDone[] = "STREAM_DONE";
    sendto(socketfd, (unsigned char *) streamDone, strlen(streamDone), 0, (const struct sockaddr *) &cliaddr, len);
    printf("End of file reached\n");


}