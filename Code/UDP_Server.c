// UDP_Server.c

//
// Created by C21Caden.Kulp on 8/29/2019.
//


//Code obtained and modified from https://www.geeksforgeeks.org/udp-server-client-implementation-c/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <errno.h>
#include "UDPServerFuncts.h"

#define PORT 4240
#define MAXLINE 1024
int main()
{
    int socketfd;  //Socket descriptor, like a file-handle
    struct sockaddr_in srvaddr, cliaddr; //Stores IP address, address family (ipv4), and port
    char buffer[MAXLINE]; //buffer to store message from client
    char *test = "testing";

    // Creating socket file descriptor
    // AF_INET = "domain" = IPv4
    // SOCK_DGRAM = "type" = UDP, unreliable
    // protocol=0, specifies UDP within SOCK_DGRAM type.
    if ((socketfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
    {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    srvaddr.sin_family = AF_INET;           // IPv4
    srvaddr.sin_addr.s_addr = INADDR_ANY;   // All available interfaces
    srvaddr.sin_port = htons( PORT );       // port, converted to network byte order (prevents little/big endian confusion between hosts)



    // Forcefully attaching socket to the port 8080
    // Bind expects a sockaddr, we created a sockaddr_in.  (struct sockaddr *) casts pointer type to sockaddr.
    if (bind(socketfd, (const struct sockaddr *)&srvaddr, sizeof(srvaddr))<0)
    {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    int len = sizeof(cliaddr), n; //must initialize len to size of buffer
    char messageToClient[101];
    char songName[101];     //Initializes string to hold song name
    char commandError[] = "COMMAND_ERROR";

    while (1) {
        printf("Waiting for connection...\n");

        // Receive message from client
        // no timeout here, always gonna wait for a datagram from the client
        if ((n = recvfrom(socketfd, (char *) buffer, MAXLINE, 0, (struct sockaddr *) &cliaddr, &len)) < 0) {
            perror("ERROR");
            printf("Errno: %d. ", errno);
            exit(EXIT_FAILURE);
        }


        // null terminate message
        buffer[n] = '\0';
        //printf("DEBUGG\tClient : %s\n", buffer);


        // Consider making this dynamic, updates the list each time it is ran
        char songList[] = "Billy Joel - We Didn't Start the Fire\nSuzanne Vega - Toms Diner";   //array to hold song list


        // List the songs to the client
        if (!strcmp(buffer, "LIST_REQUEST")) {
            printf("List request received!\n");
            snprintf(messageToClient, 101, "LIST_REPLY\n%s", songList);
            sendto(socketfd, (const char *) messageToClient, strlen(songList), 0, (const struct sockaddr *) &cliaddr, len);
            printf("MP3 list sent to client\n");
        }


        //attempt at sending data back to client when they request a song to be played
        else if (!strncmp(buffer, "START_STREAM", 12)) {
            strcpy(songName, buffer + (13 * sizeof(char)) );    //Puts song name file entered by user into songName
            /*
            fopen(songName, "rb");
            if (songName == NULL) {
                puts("Error while opening file");
                exit(-1);
            }
             */
            // test
            //sendto(socketfd, (const char *) test, strlen(test), 0, (const struct sockaddr *) &cliaddr, len);
            // trying to send file
            printf("Start Stream received.  Song name: %s\n", songName);
            readMP3(songName, socketfd, cliaddr, len);

            // read to the first frame
            // make a while loop to read and send the file
            //store the first frame in a string

        }
        else {
            printf("command error\n");
            sendto(socketfd, (const char *) commandError, strlen(test), 0, (const struct sockaddr *) &cliaddr, len);

        }

        // Respond to client
        //sendto(socketfd, (const char *)hello, strlen(hello), 0, (const struct sockaddr *) &cliaddr, len);
        //printf("Hello message sent.\n");
    }
    close(socketfd); // Close socket
    return 0;
}