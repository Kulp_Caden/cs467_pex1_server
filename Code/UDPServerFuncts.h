// UPDServerFuncts.h

//
// Created by C21Caden.Kulp on 9/3/2019.
//

#ifndef UDP_SERVER_UDPSERVERFUNCTS_H
#define UDP_SERVER_UDPSERVERFUNCTS_H

#include <stdint.h>
#include <stdio.h>
#include <sys/socket.h>
#include <netinet/in.h>

FILE* OpenMP3File(char* inputBuffer);
int CalcFrameSize(unsigned char byte);
void readMP3(char* filename, int socketfd, struct sockaddr_in cliaddr, int len );
int CalcFrameSize(unsigned char byte);

#endif //UDP_SERVER_UDPSERVERFUNCTS_H
